#include <iostream>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <math.h>
using namespace std;

int maxN(int *a,int l,int r)
{
    //单个元素时自身最大
    if(l==r)
        return a[l];
    //两个元素时返回二者中的最大元素
    else if(l+1==r)
        return (a[l] > a[r]) ? a[l] : a[r];
    //两个元素以上进行分治递归
    else
        return (maxN(a, l, (l + r) / 2) > maxN(a, (l + r) / 2 + 1, r)) ? maxN(a, l, (l + r) / 2) : maxN(a, (l + r) / 2 + 1, r);
}

int main() 
{
    int n, i;
    cout << "输入集合元素个数：";
    cin >> n;
    int *a = new int[n];
    cout << "数据集合元素：";
    for (i = 0; i < n; i++)
	{
		cin >> a[i];
	}
    int m;
    m = maxN(a, 0, n-1);
    cout << "集合最大元为：";
    cout << m;
    return 0;
}