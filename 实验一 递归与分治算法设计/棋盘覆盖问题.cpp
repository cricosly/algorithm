#include <iostream>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <math.h>
using namespace std;

const int N = 520;
int board[N][N];
int tile = 1;//L型骨牌，编号从 1 开始

//当前棋盘左上角方格的行号 tr、列号 tc，特殊方格的行号 dr、列号 dc， 棋盘规模size
void chessBoard(int tr, int tc, int dr, int dc, int size)
{
    if (size == 1)
    {
        return;
    }

    int t = tile++;   //L型骨牌号
    int s = size / 2; //分割棋盘
    
    if (dr < tr + s && dc < tc + s)//特殊方格在此棋盘中（左上角棋盘）
    {                                  
        chessBoard(tr, tc, dr, dc, s); //覆盖左上角子棋盘
    }
    else//此棋盘中无特殊方格（左上角棋盘）
    {                                                  
        board[tr + s - 1][tc + s - 1] = t;//用 t号L型骨牌覆盖右下角
        chessBoard(tr, tc, tr + s - 1, tc + s - 1, s); //将已经覆盖的方格作为特殊方格，覆盖其余方格
    }

    if (dr < tr + s && dc >= tc + s)//特殊方格在此棋盘中（右上角）
    {                                      
        chessBoard(tr, tc + s, dr, dc, s); //覆盖右上角子棋盘
    }
    else//此棋盘中无特殊方格（右上角）
    {                                                  
        board[tr + s - 1][tc + s] = t;//用 t号L型骨牌覆盖左下角
        chessBoard(tr, tc + s, tr + s - 1, tc + s, s); //将已经覆盖的方格作为特殊方格，覆盖其余方格
    }

    if (dr >= tr + s && dc < tc + s) //特殊方格在此棋盘中（左下角）
    {                                      
        chessBoard(tr + s, tc, dr, dc, s);//覆盖左下角子棋盘
    }
    else//此棋盘中无特殊方格（左下角）
    {                                                  
        board[tr + s][tc + s - 1] = t;//用 t号L型骨牌覆盖右上角
        chessBoard(tr + s, tc, tr + s, tc + s - 1, s);//将已经覆盖的方格作为特殊方格，覆盖其余方格
    }

    if (dr >= tr + s && dc >= tc + s)//特殊方格在此棋盘中（右下角）
    {                                          
        chessBoard(tr + s, tc + s, dr, dc, s);//覆盖右下角子棋盘
    }
    else//此棋盘中无特殊方格（右下角）
    {
        board[tr + s][tc + s] = t;//用 t号L型骨牌覆盖左上角
        chessBoard(tr + s, tc + s, tr + s, tc + s, s);//将已经覆盖的方格作为特殊方格，覆盖其余方格
    }
}

int main()
{
    int k, dr, dc, size;
    while (cin >> k)//k表示阶数,可以同时对多个不同的棋盘进行覆盖
    {                    
        size = (int)pow(2, k); //棋盘规模size=2^k
        cin >> dr >> dc; //特殊方格的行号 dr、列号 dc
        board[dr][dc] = 0;//特殊方格用 0 标记

        chessBoard(1, 1, dr, dc, size);

        for (int i = 1; i <= size; ++i)
        {
            for (int j = 1; j <= size; ++j)
            {
                cout << left << setw(5) << board[i][j];
            }
            cout << endl;
        }
    }
    return 0;
}
