# NEFU-算法设计与分析-实验
![输入图片说明](resource/%E5%9B%BE%E7%89%87.jpg)
#### 介绍
NEFU计控学院的大二下学期，基于C/C++的算法设计与分析 ，四个实验，仓库备份

#### 环境说明
操作系统：Windows 10<br>
IDE：Visual Studio Code、Dev C++ 5.11、Code::Blocks

  

-  **实验一 递归与分治算法设计** 

> 1. 棋盘覆盖问题
> 2. 合并排序问题
> 3. 集合最大元问题

-  **实验二 动态规划算法设计** 

> 1. 数字三角问题
> 2. 最长公共子序列问题
> 3. 日常购物

-  **实验三 贪心算法设计** 

> 1. 最优服务次序问题
> 2. 区间相交问题
> 3. 汽车加油问题

-  **实验四 回溯算法设计** 

> 1. 0-1背包问题
> 2. 旅行售货员问题

#### 食用建议

1.  遵循良好的命名规范，使用有意义且易于理解的变量名、函数名和类名
2.  遵循一致的代码风格，如缩进、空格、括号的使用等
3.  有发现错误，欢迎留言


