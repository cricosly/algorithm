#include <iostream>
using namespace std;
#define M 1024
int main() {
    int n, k, d[M];
    cin >> n >> k;
    int l = 0, num = 0; //l记录当前走过的路程，n表示一次加满油能走的最远路程，num记录加油次数
    for (int i = 1; i <= k + 1; i++)
    {
        cin >> d[i];
        if(d[i] > n) { //无法到达下一个加油站
            cout << "No Solution" << endl;
            return 0;
        }
        if(l + d[i] > n) { //必须在当前加油站加油
            l = d[i]; //更新当前位置，加油后汽车能行驶最大距离变成 n，所以当前走过的路程更新为 d[i]
            num++;//加油次数加1
        }
        else {          //尽量往远处走
            l += d[i]; //更新当前位置，当前走过的路程 + d[i]
        }
    }
    cout << num << endl;
    return 0;
}
/*
例如： 
n=7  k=7
K+1个整数：1 2 3 4 5 1 6 6 
最优值：4

*/