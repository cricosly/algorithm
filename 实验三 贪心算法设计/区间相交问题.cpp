#include <iostream>
#include <algorithm>
using namespace std;

const int M = 1024;

struct Qu {
    int l, r;
}I[M];

bool cmp(Qu x, Qu y) { //按右端点排序
    return x.r < y.r;
}

int main() {
    int n, l, r;
    cin >> n;
    for(int i = 1; i <= n; i++) {
        cin >> l >> r;
        if(l<=r)
        {
            I[i].l = l;
            I[i].r = r;
        }
        else
        {
            I[i].l = r;
            I[i].r = l;
        }  
    }
    //sort(begin, end, cmp)，
    //begin为指向待sort()的数组的第一个元素的指针
    //end为指向待sort()的数组的最后一个元素的下一个位置的指针
    //cmp参数为排序准则，cmp参数可以不写，如果不写的话，默认从小到大进行排序。
    sort(I + 1, I + n + 1, cmp);
    
    int cnt = 1, pre = I[1].r;
    for(int i = 2; i <= n; i++) {
        if(I[i].l > pre) { //当前区间与前一个不相交
            cnt++;//cnt个区间不相交
            pre = I[i].r; //更新右端点
        }
    }
    cout << n - cnt << endl;//输出去掉的最少闭区间数
    return 0;
}
