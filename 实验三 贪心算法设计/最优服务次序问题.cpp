#include <iostream>
#include <stack>
#include <queue>
#include <iomanip>
#define maxsize 1000
using namespace std;
typedef struct{
    int t[maxsize+1];  //存每个人服务时间
    int w[maxsize + 1];//存每个人等待服务时间
    double avgw;//存平均等待时间
    int n;
}ServeList;

void InsertSort(ServeList &L)//直接插入排序
{
    for(int i=2; i<=L.n; i++)
        if(L.t[i] < L.t[i-1])
        {
            L.t[0] = L.t[i];//r[0]存为哨兵单元
            L.t[i] = L.t[i-1];
            int j;
            for (j = i - 2; L.t[0] < L.t[j]; j--)
                L.t[j+1] = L.t[j];
            L.t[j+1] = L.t[0];
        }
}

void minwait(ServeList &L)
{
    L.t[0] = 0;
    L.w[0] = 0;
    L.avgw = 0;
    int i;
    for (i = 1; i <= L.n; i++)
    {
        L.w[i] = L.t[i] + L.w[i - 1];//当前人的等待服务时间 = 前一个人的等待服务时间 + 当前人的服务时间
        L.avgw += L.w[i];
    }
    L.avgw = L.avgw / L.n;
    cout << "最小平均等待时间:";
    cout << fixed << setprecision(2) << L.avgw;
}

int main()
{
    ServeList L;
    int x, i, n;
    cin >> n;
    L.n = n;
    i = 1;
    while(i<=n)
    {
        cin >> x;
        L.t[i] = x;//从t[1]开始存数
        i++;
    }
    InsertSort(L);
    /*for(int i=1; i<=L.n; i++)
        cout<<L.t[i]<<" ";*/
    minwait(L);
    return 0;
}

/*
10
56 12 1 99 1000 234 33 55 99 812
*/