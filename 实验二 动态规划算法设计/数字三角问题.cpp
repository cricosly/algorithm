#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <math.h>
using namespace std;

#define N 1024
int d[N][N]; // 指标函数d（i，j）为从格子（i，j）出发时能得到的最大和（包括格子（i，j）本身的值）
// 本题的解为d[1][1]
int a[N][N];
int main()
{
	int n, i, j;
	cin >> n;
	for (i = 1; i <= n; i++) // 1-n行
	{
		for (j = 1; j <= i; j++) // 1-i列
		{
			cin >> a[i][j];	   // 输入三角形各个位置的值
			d[i][j] = a[i][j]; // 指标函数d（i，j）赋初值
		}
	}
	for (i = n - 1; i >= 1; i--) // 从n-1行开始计算指标函数d（i，j）值
	{
		for (j = 1; j <= i; j++)
		{
			d[i][j] += max(d[i + 1][j], d[i + 1][j + 1]);
		}
	}
	cout << d[1][1];
	return 0;
}